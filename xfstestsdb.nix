{pkgs, ...}: let
  pypkgs = {python311Packages}:
    with python311Packages; [
      setuptools
      pyxdg
      pygobject3
      termcolor
      dateutil
    ];
in
  pkgs.python311Packages.buildPythonApplication {
    pname = "xfstestsdb";
    version = "1.7-git";
    src = pkgs.fetchgit {
      url = "https://git.nowheycreamery.com/anna/xfstestsdb.git";
      rev = "a00758f66d";
      sha256 = "sha256-931dC/Izq+oCgrr4pwzPpOqpErbZ4A/sybC2EwCTXnA=";
    };
    doCheck = false;
    nativeBuildInputs = with pkgs; [
      wrapGAppsHook4
      gobject-introspection
      libadwaita
      gtk4
    ];
    dontWrapGApps = true;

    # Arguments to be passed to `makeWrapper`, only used by buildPython*
    preFixup = ''
      makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
    '';
    propagatedBuildInputs = with pkgs; [
      wrapGAppsHook4
      gobject-introspection
      libadwaita
      gtk4
      gsettings-desktop-schemas
      (pkgs.python311.withPackages (ps:
        pypkgs {
          inherit
            (pkgs)
            python311Packages
            ;
        }))
    ];
  }
