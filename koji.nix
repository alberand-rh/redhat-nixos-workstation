# Create a config in ~/.koji/config:
#
# [koji]
# server = https://brewhub.engineering.redhat.com/brewhub
# authtype = kerberos
# krbservice = brewhub
# topdir = /mnt/redhat/brewroot
# weburl = https://brewweb.engineering.redhat.com/brew
# topurl = http://download.devel.redhat.com/brewroot
# use_fast_upload = yes
# anon_retry = yes
# serverca = /etc/pki/tls/certs/ca-bundle.crt
# poll_interval = 11
{pkgs}:
pkgs.python311Packages.buildPythonApplication {
  name = "koji";

  src = pkgs.fetchgit {
    url = "https://pagure.io/koji.git";
    hash = "sha256-j6lpy4uXkUd5CTbn/kP4jzDDYuXmY0ZkwTnMMPqCcJ0=";
  };

  propagatedBuildInputs = with pkgs.python311Packages; [
    six
    dateutil
    requests
    defusedxml
    requests-gssapi
  ];

  doCheck = false;
}
