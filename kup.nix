{ stdenv, fetchgit, openssh, perlPackages, makeWrapper }:

let
  deps = with perlPackages; [
    ConfigSimple
    Encode
    PathTools
  ];
in stdenv.mkDerivation {
  pname = "kup";
  version = "0.3.6";

  src = fetchgit {
    url = "git://git.kernel.org/pub/scm/utils/kup/kup.git";
    hash = "sha256-dMLeSAGo8cTQs9ewVhSflD6bIPH0TfN6hgT97rv/SdY=";
  };

  propagateBuildInputs = deps;

  buildInputs = [
    makeWrapper
  ] ++ deps;

  patchPhase = ''
    sed -i 's~/usr/bin/perl -T~/usr/bin/env perl~' kup genrings gpg-sign-all kup-server
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp genrings $out/bin
    cp gpg-sign-all $out/bin
    cp kup $out/bin
    cp kup-server $out/bin
  '';

  postFixup = ''
    wrapProgram $out/bin/kup \
      --set KUP_RSH "${openssh}/bin/ssh" \
      --prefix PERL5LIB : "${with perlPackages; makePerlPath deps}"
  '';
}
