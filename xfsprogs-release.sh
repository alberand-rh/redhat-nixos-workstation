#!/usr/bin/env sh
#
# Script for releasing xfsprogs-dev package.
#
# Repository:
# https://git.kernel.org/pub/scm/fs/xfs/xfsprogs-dev.git/
#
# Author: Carlos Maiolino <cem@kernel.org>

SOURCE="$PWD"
VERSION_FILE="$SOURCE/VERSION"
CONFIGURE_FILE="$SOURCE/configure.ac"
DEB_FILE="$SOURCE/debian/changelog"
LIST="linux-xfs@vger.kernel.org"
FORNEXT_DIR="~/Source/xfsprogs/for-next"
LAST_HEAD=""
DEBUG=1

VER_MAJ="$1"
VER_MIN="$2"
VER_REV="$3"

CHANGELOG_FILE="/tmp/changelog.$$"

##### COMMON #####

# Print Usage information
usage(){
	echo "$(basename) [-n|-r] <maj_ver> <min_ver> <revision>"
	echo "\t-n for-next email"
	echo "\t-r release email"
	exit
}

# Check update versioning
check_args(){
	if [ -z ${VER_MAJ} ]; then
		echo "SET MAJ VERSION"
		usage
	fi
	if [ -z ${VER_MIN} ]; then
		echo "SET MIN VERSION"
		usage
	fi
	if [ -z ${VER_REV} ]; then
		echo "SET REVISION"
		usage
	fi
}

# Open neomutt with the composed message to send
send_email(){
SUBJECT="$1"
BODY="$2"

	if [ -z "$SUBJECT" ]; then
		echo "No subject... exiting"
		exit 1
	fi

	if [ -f $BODY ]; then
		neomutt -F ~/.mutt/muttrc-kernel -s "$SUBJECT" $LIST -i $BODY
	else
		echo "No message body... exiting"
		exit 1
	fi

}

print_shortlog(){
LAST_HEAD=$1
HEAD=$2

	if [ -z $LAST_HEAD ]; then
		echo "Previous head not set... exiting"
		exit 1
	fi

	if [ -z $HEAD ]; then
		HEAD="HEAD"
	fi

	echo "$(git shortlog --format="[%h] %s" $LAST_HEAD..$HEAD)"
}

print_diffstat(){
LAST_HEAD=$1
HEAD=$2

	if [ -z $LAST_HEAD ]; then
		echo "Previous head not set... exiting"
		exit 1
	fi

	if [ -z $HEAD ]; then
		HEAD="HEAD"
	fi

	echo "$(git diff --stat --summary -C -M $LAST_HEAD..$HEAD)"
}

print_commit_count(){
LAST_HEAD=$1
HEAD=$2

	if [ -z $LAST_HEAD ]; then
		echo "Previous head not set... exiting"
		exit 1
	fi

	if [ -z $HEAD ]; then
		HEAD="HEAD"
	fi

	echo "$(git log --oneline $LAST_HEAD.. | wc -l)"
}

print_head(){
	if [ "$1" == "short" ]; then
		HEAD=$(git log --oneline --format="%h" -1)
	else
		HEAD=$(git log --oneline --format="%H" -1)
	fi
	echo "$HEAD"
}

##### COMMON END #####


##### FOR-NEXT UPDATE #####
compose_fornext_email(){

	MAIL_FILE=$(mktemp)
	LAST_HEAD=$1

if [ -z $LAST_HEAD ]; then
	echo "compose_email: Previous head not set... exiting"
	exit 1
fi

# BEGIN_OF_MESSAGE
cat << EOF > $MAIL_FILE
Hello.

The xfsprogs for-next branch, located at:

https://git.kernel.org/pub/scm/fs/xfs/xfsprogs-dev.git/refs/?h=for-next

Has just been updated.

Patches often get missed, so if your outstanding patches are properly reviewed on
the list and not included in this update, please let me know.

The new head of the for-next branch is commit:

$(print_head)

$(print_commit_count $LAST_HEAD) new commits:

$(print_shortlog $LAST_HEAD)

Code Diffstat:

$(print_diffstat $LAST_HEAD)
EOF
# END_OF_MESSAGE

echo $MAIL_FILE
}

compose_release_email(){

	MAIL_FILE=$(mktemp)
	LAST_HEAD=$1

cat << EOF > $MAIL_FILE
Hi folks,

The xfsprogs repository at:

	git://git.kernel.org/pub/scm/fs/xfs/xfsprogs-dev.git

has just been updated.

Patches often get missed, so if your outstanding patches are properly reviewed
on the list and not included in this update, please let me know.

The for-next branch has also been updated to match the state of master.

The new head of the master branch is commit:

$(print_head)

New commits:

$(print_shortlog $LAST_HEAD)

Code Diffstat:

$(print_diffstat $LAST_HEAD)
EOF

#END_OF_MESSAGE
echo $MAIL_FILE

}
fornext_announce(){

	LAST_HEAD=$1

	if [ -z $LAST_HEAD ]; then
		echo "No starting HEAD specified... exiting"
		exit 1
	fi

	SUBJECT="[ANNOUNCE] xfsprogs: for-next updated to $(print_head short)"
	BODY=$(compose_fornext_email $LAST_HEAD)


	echo "$SUBJECT"
	echo "$BODY"

	# send_email "$SUBJECT" $BODY
}

release_announce(){
	LAST_HEAD=$1
	RELEASE=$(git describe --abbrev=0)

	if [ -z $LAST_HEAD ]; then
		echo "No starting HEAD specified... exiting"
		exit 1
	fi

	SUBJECT="[ANNOUNCE] xfsprogs $RELEASE released"
	BODY=$(compose_release_email $LAST_HEAD)

	echo "$SUBJECT"
	echo "$BODY"
	# send_email "$SUBJECT" $BODY
}
##### FOR-NEXT UPDATE #####

##### RELEASE SETUP ######
update_version_file(){
	echo "updating version"
	sed -i -e "s/^PKG_MAJOR=.*/PKG_MAJOR=$VER_MAJ/" $VERSION_FILE
	sed -i -e "s/^PKG_MINOR=.*/PKG_MINOR=$VER_MIN/" $VERSION_FILE
	sed -i -e "s/^PKG_REVISION=.*/PKG_REVISION=$VER_REV/" $VERSION_FILE
}

update_configure_file(){
	CONF_AC="AC_INIT([xfsprogs],[$VER_MAJ.$VER_MIN.$VER_REV],[linux-xfs@vger.kernel.org])"

	sed -i "s/^AC_INIT.*/$CONF_AC/" $CONFIGURE_FILE
}

update_debian_changelog(){
#if [ ! -s $CHANGELOG_FILE ]; then
#	echo "Error: changelog does not exist or empty. Exiting..."
#	exit
#fi

sed -i "1s/^/\n/" $DEB_FILE
sed -i "1s/^/ -- Nathan Scott <nathans@debian.org>  `date -R`\n/" $DEB_FILE
sed -i "1s/^/\n/" $DEB_FILE

#while read -r LINE; do
#	sed -i "1s/^/  * $LINE\n/" $DEB_FILE
#done <$CHANGELOG_FILE

sed -i "1s/^/  * New upstream release\n/" $DEB_FILE
sed -i "1s/^/\n/" $DEB_FILE
sed -i "1s/^/xfsprogs ($VER_MAJ.$VER_MIN.$VER_REV) unstable; urgency=low\n/" $DEB_FILE
}

# Get user inputs for each changelog entry
# XXX this should be automated based on patches subjects
get_changelog(){
	LOG=""
	while [ 1 ]; do
		read -p "Changelog entry:" tmplog

		if [ -z "$tmplog" ]; then
			break;
		fi
		echo "$tmplog" >> $CHANGELOG_FILE
	done
}


#MOVE TO THE CORRECT REPO
#cd $FORNEXT_DIR

#check_args
#get_changelog
#update_version_file
#update_configure_file
#update_debian_changelog
#rm $CHANGELOG_FILE
#email_fornext $1
#fornext_announce 37e6e80a6

while getopts "n:r:" opt; do
	case $opt in
		n)	# for-next
			echo "Creating for-next update email"
			LAST_HEAD=$OPTARG
			fornext_announce $LAST_HEAD
			;;
		r)	# Release
			echo "Creating release email"
			LAST_HEAD=$OPTARG
			release_announce $LAST_HEAD
			;;
		*)
			usage
			;;
	esac
done
