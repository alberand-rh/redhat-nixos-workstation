# Example configuration is in configs/beaker.conf
{pkgs, ...}: let
  beaker-common = with pkgs.python311Packages;
    buildPythonPackage rec {
      pname = "beaker-common";
      version = "29.1";
      src = fetchPypi {
        inherit pname version;
        sha256 = "sha256-g/YqlJDF0373NvNW+u8Jc4/jqwRSvSq/snOzi6NT9bI=";
      };
      doCheck = false;
    };
in
  pkgs.python311Packages.buildPythonPackage rec {
    pname = "beaker-client";
    version = "29.1";
    src = pkgs.python311Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-8RTaK6u5VMaaDpZOln0yO0QN9r6cNO/FsZnSaPmKFvc=";
    };
    doCheck = false;
    propagatedBuildInputs = with pkgs.python311Packages; [
      beaker-common
      six
      lxml
      requests
      prettytable
      jinja2
      gssapi
      setuptools
    ];
  }
