{
  description = "Red Hat Workstation packages";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      koji-drv = import ./koji.nix {inherit pkgs;};
      revumatic-drv = import ./revumatic.nix {inherit pkgs;};
      kerneloscope-drv = import ./kerneloscope.nix {inherit pkgs;};
      beaker-client-drv = import ./beaker-client.nix {inherit pkgs;};
      kup-drv = import ./kup.nix {
        inherit
          (pkgs)
          stdenv
          fetchgit
          perlPackages
          openssh
          makeWrapper
          ;
      };
      xfstestsdb-drv = import ./xfstestsdb.nix {inherit pkgs;};
    in {
      packages = {
        koji = koji-drv;
        revumatic = revumatic-drv;
        kerneloscope = kerneloscope-drv;
        beaker-client = beaker-client-drv;
        kup = kup-drv;
        xfsprogs-release = (pkgs.writeShellScriptBin "xfsprogs-release"
          (builtins.readFile ./xfsprogs-release.sh));
        xfstestsdb = xfstestsdb-drv;
        default = revumatic-drv;
      };

      apps = rec {
        koji = flake-utils.lib.mkApp {
          drv = self.packages.${system}.koji;
        };
        revumatic = flake-utils.lib.mkApp {
          drv = self.packages.${system}.revumatic;
        };
        kerneloscope = flake-utils.lib.mkApp {
          drv = self.packages.${system}.kerneloscope;
        };
        beaker-client = flake-utils.lib.mkApp {
          drv = self.packages.${system}.beaker-client;
        };
        kup = flake-utils.lib.mkApp {
          drv = self.packages.${system}.kup;
        };
        xfsprogs-release = flake-utils.lib.mkApp {
          drv = self.packages.${system}.xfsprogs-release;
        };
        xfstestsdb = flake-utils.lib.mkApp {
          drv = self.packages.${system}.xfstestsdb;
        };
        default = revumatic;
      };
    });
}
