{pkgs}:
pkgs.stdenv.mkDerivation {
  name = "kerneloscope";
  src = pkgs.fetchgit {
    url = "https://gitlab.cee.redhat.com/kernel-review/kerneloscope.git";
    hash = "sha256-fGxEOhR9qbwWTanaCYsWhOHMmNk+1Jq/8i9pBbZ9Z4A=";
  };
  nativeBuildInputs = [pkgs.installShellFiles];
  propagatedBuildInputs = [
    pkgs.python311
  ];
  installPhase = ''
    install -Dm755 client/kerneloscope $out/bin/kerneloscope
    runHook postInstall
  '';
  postInstall = ''
    installShellCompletion ${./configs/kerneloscope.zsh}
  '';
}
