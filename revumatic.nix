{ pkgs }:
pkgs.python311Packages.buildPythonApplication rec {
  name = "revumatic";
  version = "23.08.31";

  src = pkgs.fetchgit {
    url = "https://gitlab.com/redhat/centos-stream/src/kernel/utils/revumatic.git";
    rev = "21d24044a728b228a4486695a0eca269fcfee805";
    hash = "sha256-BYv/2M4rVORLpUCJJHGfVvStgMQklbcMZhAES2NoFwU=";
  };

  postPatch = ''
    substituteInPlace setup.py \
      --replace "version=version.get_version()," "version='${version}',"
  '';

  propagatedBuildInputs = with pkgs.python311Packages; [
    pygit2
    levenshtein
    pyyaml
    urllib3
  ];

  doCheck = false;
}
